#!/usr/bin/perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This program comes with ABSOLUTELY NO WARRANTY;

use Modern::Perl;

use Getopt::Long;
use Pod::Usage;
use File::chdir;
use FindBin;
use lib "$FindBin::Bin/lib";

use KohaI18N;

my ( $help, $koha_dir, $koha_l10n_dir, $git_branch, $lang_filter, );
GetOptions(
    'h|help'          => \$help,
    'koha-dir=s'      => \$koha_dir,
    'koha-l10n-dir=s' => \$koha_l10n_dir,
    'branch=s'        => \$git_branch,
    'lang=s'          => \$lang_filter,
) || pod2usage(1);

pod2usage( -verbose => 2 ) if $help;

pod2usage("koha-dir does not exist. It must point to the koha repository.")
  unless $koha_dir;
pod2usage(
    "koha-l10n-dir does not exist. It must point to the koha-l10n repository.")
  unless $koha_l10n_dir;

my $project = 'koha';
$git_branch ||= 'master';

$CWD = $koha_dir;

my $i18n = KohaI18N->new(
    {
        project       => $project,
        git_branch    => $git_branch,
        koha_dir      => $koha_dir,
        koha_l10n_dir => $koha_l10n_dir,
        lang_filter   => $lang_filter,
    }
);

for my $dir ( ( $koha_dir, $koha_l10n_dir ) ) {
    my $r =
      $i18n->run_cmd( sprintf( q{git -C %s diff}, $dir ), { silent => 1 } );
    if ( $r->{output} ) {
        say sprintf
"Cannot run: You have unstaged changes in %s.\nPlease commit or stash them.",
          $dir;
        exit 1;
    }
}

my @langs = $i18n->get_langs;
die "no lang or not valid lang passed" unless @langs;

$i18n->update_git_repo($koha_dir, $git_branch);
run_cmd q{yarn install};

my $po_list = $i18n->get_po_list();

$i18n->lock_weblate();

my $repo_object = $project . '/' . $git_branch . '%252F' . 'installer';

$i18n->run_cmd(qq{wlc push $repo_object});
$i18n->update_git_repo($koha_l10n_dir, $git_branch);

for my $koha_lang_code ( sort @langs ) {
    $i18n->run_cmd(
        sprintf(
            qq{cp %s/%s*.po %s},
            $koha_l10n_dir, $koha_lang_code, $i18n->{koha_po_dir}
        )
    );
}

$i18n->translate_update_koha();

$i18n->fix_koha_version($po_list);
$i18n->fix_xss($po_list);

$i18n->run_cmd( sprintf( qq{cp %s $koha_l10n_dir}, $po_list ) );

$CWD = $koha_dir;
my $r        = $i18n->run_cmd( q{git rev-parse --short HEAD}, { silent => 1 } );
my $commitid = $r->{output};

$CWD = $koha_l10n_dir;

# Do not die, maybe there is nothing to commit
$i18n->run_cmd( qq{git commit -a -m"Update from Koha - $commitid"},
    { do_not_die => 1 } );

$i18n->run_cmd(qq{git push origin $git_branch});

$CWD = $koha_dir;
$i18n->run_cmd(qq{git reset --hard HEAD});

$i18n->run_cmd(qq{wlc pull $repo_object});

END {
    $i18n->unlock_weblate();
}
