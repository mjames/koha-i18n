package KohaI18N;

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This program comes with ABSOLUTELY NO WARRANTY;

use Modern::Perl;
use POSIX qw( strftime );
use Array::Utils qw( intersect );
use File::Basename qw( dirname );
use File::chdir;
use File::Slurp qw( read_dir );
use IPC::Cmd qw( run );
use Locale::PO;
use JSON qw( decode_json from_json );

sub new {
    my ( $class, $params ) = @_;
    my $self = {
        project       => $params->{project},
        git_branch    => $params->{git_branch},
        koha_dir      => $params->{koha_dir},
        koha_l10n_dir => $params->{koha_l10n_dir},
        lang_filter   => $params->{lang_filter},
    };

    $self->{koha_po_dir} = $self->{koha_dir} . "/misc/translator/po";
    bless( $self, $class );
    return $self;
}

my $w2k = {
    "am_ETHI"    => "am-Ethi",
    "ar_ARAB"    => "ar-Arab",
    "as"         => "as-IN",
    "az"         => "az-AZ",
    "be"         => "be-BY",
    "bg_CYRL"    => "bg-Cyrl",
    "bn_IN"      => "bn-IN",
    "ca"         => "ca-ES",
    "cs"         => "cs-CZ",
    "cy"         => "cy-GB",
    "da"         => "da-DK",
    "de_CH"      => "de-CH",
    "de"         => "de-DE",
    "dz"         => "dz",
    "el"         => "el-GR",
    "en_GB"      => "en-GB",
    "en_NZ"      => "en-NZ",
    "eo"         => "eo",
    "es"         => "es-ES",
    "et"         => "et",
    "eu"         => "eu",
    "fa_ARAB"    => "fa-Arab",
    "fi"         => "fi-FI",
    "fo"         => "fo-FO",
    "fr_CA"      => "fr-CA",
    "fr"         => "fr-FR",
    "ga"         => "ga",
    "gd"         => "gd",
    "gl"         => "gl",
    "he_HEBR"    => "he-Hebr",
    "hi"         => "hi",
    "hr"         => "hr-HR",
    "hu"         => "hu-HU",
    "hy_ARMN"    => "hy-Armn",
    "hyw_ARMN"   => "hyw-Armn",
    "ia"         => "ia",
    "id"         => "id-ID",
    "iq_CA"      => "iq-CA",
    "is"         => "is-IS",
    "it"         => "it-IT",
    "iu"         => "iu-CA",
    "ja_JPAN-JP" => "ja-Jpan-JP",
    "ka"         => "ka",
    "km"         => "km-KH",
    "kn_KNDA"    => "kn-Knda",
    "ko_KORE-KP" => "ko-Kore-KP",
    "ku_ARAB"    => "ku-Arab",
    "lo_LAOO"    => "lo-Laoo",
    "lv"         => "lv",
    "mi"         => "mi-NZ",
    "ml"         => "ml",
    "mnw"        => "mon",
    "mr"         => "mr",
    "ms"         => "ms-MY",
    "my"         => "my",
    "nb_NO"      => "nb-NO",
    "ne_NE"      => "ne-NE",
    "nl_BE"      => "nl-BE",
    "nl"         => "nl-NL",
    "nn"         => "nn-NO",
    "oc"         => "oc",
    "pbr"        => "pbr",
    "pl"         => "pl-PL",
    "prs"        => "prs",
    "pt_BR"      => "pt-BR",
    "pt_PT"      => "pt-PT",
    "ro"         => "ro-RO",
    "ru"         => "ru-RU",
    "rw"         => "rw-RW",
    "sd_PK"      => "sd-PK",
    "si"         => "si",
    "sk"         => "sk-SK",
    "sl"         => "sl-SI",
    "sq"         => "sq-AL",
    "sr_Cyrl"    => "sr-Cyrl",
    "sv"         => "sv-SE",
    "sw_KE"      => "sw-KE",
    "ta_LK"      => "ta-LK",
    "ta"         => "ta",
    "te"         => "te",
    "tet"        => "tet",
    "th"         => "th-TH",
    "tk"         => "tk",
    "tl"         => "tl-PH",
    "tr"         => "tr-TR",
    "tvl"        => "tvl",
    "uk"         => "uk-UA",
    "ur_ARAB"    => "ur-Arab",
    "uz"         => "uz",
    "vi"         => "vi-VN",
    "zh_Hans"    => "zh-Hans-CN",
    "zh_Hant"    => "zh-Hant-TW",
};

my $k2w = { reverse %$w2k };

my @all_langs = keys %$k2w;

sub get_langs {
    my ($self) = @_;

    unless ( exists $self->{_langs} ) {
        my @langs;
        if ( $self->{lang_filter} ) {
            my @lang_filters = split( ',', $self->{lang_filter} );
            @langs = intersect @all_langs, @lang_filters;
        }
        else {
            @langs = @all_langs;
        }
        $self->{_langs} = \@langs;
    }
    return @{ $self->{_langs} };
}

sub run_cmd {
    my $self       = shift;
    my $cmd        = shift;
    my $params     = shift;
    my $silent     = $params->{silent}     || 0;
    my $do_not_die = $params->{do_not_die} || 0;

    my ( $success, $error_code, $full_buf, $stdout_buf, $stderr_buf ) =
      run( command => $cmd, verbose => !$silent );
    die sprintf "Command failed with %s\n", $error_code
      unless $do_not_die || $success;
    my $output = "@$full_buf";
    chomp $output;
    return { output => $output, error_code => $error_code };
}

sub update_git_repo {
    my ($self) = @_;
    $self->run_cmd(q{git stash});
    $self->run_cmd(q{git reset --hard HEAD});
    $self->run_cmd(q{git fetch origin});

    $self->run_cmd(q{git clean -f -d -q});
    $self->run_cmd( sprintf q{git checkout -B %s origin/%s},
        $self->{git_branch}, $self->{git_branch} );
}

sub get_components {
    my ($self) = @_;
    unless ( exists $self->{_components} ) {
        my $r = $self->run_cmd(
            sprintf(
                q{wlc --format json list-components %s}, $self->{project}
            ),
            { silent => 1 }
        );
        my $components = from_json $r->{output};
        $self->{_components} =
          [ map { $_->{is_glossary} ? () : $_->{name} || () } @$components ];
    }
    return @{ $self->{_components} };
}

sub translate_update_koha {
    my ($self) = @_;
    $CWD = $self->{koha_dir};
    $self->l("Extracting new strings into .pot files");
    $self->run_cmd(q{gulp po:extract});
    $self->l("Updating Koha .po files");
    $self->run_cmd( q{gulp po:update --generate-pot=never --lang }
          . join( ' --lang ', $self->get_langs ) );
}

sub get_po_list {
    my ($self) = @_;

    my $dir   = $self->{koha_po_dir};
    my @langs = $self->get_langs;
    return "$dir/*.po"
      if @langs == @all_langs;

    return join( " ", map { sprintf "%s/%s-*.po", $dir, $_ } @langs );
}

sub fix_koha_version {
    my ( $self, $files ) = @_;
    my $r            = $self->run_cmd(q{perl -MKoha -e "print Koha::version"});
    my $koha_version = $r->{output};
    $self->run_cmd(
        sprintf(
q{perl -p -i -e 's#"Project-Id-Version:.*\\\n"\n#"Project-Id-Version: Koha %s\\\n"\n#' %s},
            $koha_version, $files
        )
    );
}

sub fix_xss {
    my ( $self, $files ) = @_;

    # Previous regex was "%\s+PERL| %\s+RAWPERL|<script"
    # Here we are more strict and reject *PERL* and *<script*
    my $r = $self->run_cmd(
        sprintf( q{egrep -l "%%\s*PERL|%%\s*RAWPERL|<script" %s}, $files ),
        { do_not_die => 1 } );
    my $bad_files = $r->{output};

    *Locale::PO::quote = sub {

        # This is coming from misc/translator/TmplTokenizer.pm sub quote_po
        # It seems that Locale::PO does not deal with tab correcty
        my ( $self, $s ) = @_;
        $s =~ s/([\\"])/\\$1/gs;
        $s =~ s/\n/\\n/g;
        return "\"$s\"";
    };
    for my $file ( split "\n", $bad_files ) {
        chomp $file;
        my $tr_array = Locale::PO->load_file_asarray($file);

        for my $translation (@$tr_array) {
            my $msgstr = $translation->{msgstr};
            if (    $msgstr
                and length($msgstr) > 5
                and $msgstr =~ /\%\s*PERL|\%\s*RAWPERL|<script/ )
            {
                $translation->fuzzy(1);
            }
        }

        Locale::PO->save_file_fromarray( $file, $tr_array );
    }
}

sub l {
    my ( $self, $msg ) = @_;
    my $t = strftime( "%Y-%m-%d %H:%M:%S", localtime );
    say sprintf "[%s] %s", $t, $msg;
}

my @locks;

sub _lock_unlock_weblate {
    my ($self)   = @_;
    my $action   = shift;
    my @po_files = $self->get_po_list;
    for my $po_file ( sort @po_files ) {
        my $object =
          $self->{project} . '/' . $self->{git_branch} . '%252F' . $po_file;

        $self->run_cmd(qq{wlc $action $object});
        push @locks, $object;
    }
}

sub lock_weblate {
    my ($self) = @_;
    $self->_lock_unlock_weblate('lock');
}

sub unlock_weblate {
    my ($self) = @_;
    $self->_lock_unlock_weblate('unlock');
}

1;
